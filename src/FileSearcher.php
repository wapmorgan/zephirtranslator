<?php
namespace wapmorgan\ZephirTranslator;

use \FilesystemIterator;
use \FilterIterator;
use \RegexIterator;
use \RecursiveIteratorIterator;
use \RecursiveDirectoryIterator;

class FileSearcher extends FilterIterator {
    public $extensions = array('php', 'php3');
    private $iterator;

    public function __construct($directory) {
        $this->iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory, FilesystemIterator::SKIP_DOTS));
    }

    public function filter() {
        $this->iterator = new RegexIterator($this->iterator, '/\.('.implode('|', $this->extensions).')$/');
    }

    public function accept() {
        return $this->iterator->accept();
    }

    public function current() {
        return $this->iterator->current();
    }

    public function getInnerIterator() {
        return $this->iterator->getInnerIterator();
    }

    public function key() {
        return $this->iterator->key();
    }

    public function next() {
        return $this->iterator->next();
    }

    public function rewind() {
        return $this->iterator->rewind();
    }

    public function valid() {
        return $this->iterator->valid();
    }

}
