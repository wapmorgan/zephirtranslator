<?php
namespace wapmorgan\ZephirTranslator;

class ZephirProject {
    public $namespace;
    public $name;
    public $description;
    public $author;
    public $version;
}