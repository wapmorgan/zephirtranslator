<?php
namespace wapmorgan\ZephirTranslator;

use PhpParser\Node;
use PhpParser\Node\Name;
use PhpParser\NodeVisitorAbstract;

class NamespaceConverter extends NodeVisitorAbstract {
    public function leaveNode(Node $node) {
        if ($node instanceof Name) {
            return new Name(ucfirst($node->toString()));
        }
    }
}
