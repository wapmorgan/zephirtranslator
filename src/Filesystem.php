<?php
namespace wapmorgan\ZephirTranslator;

class Filesystem {
    static public function isEmptyDir($dir) {
        return count(glob($dir.'/*')) + count(glob($dir.'/.*')) === 2;
    }

    static public function deleteDir($dir) {
        $dd = opendir($dir);
        while (($file = readdir($dd)) !== false) {
            if (in_array($file, array('.', '..'))) continue;

            if (is_file($dir.'/'.$file) || is_link($dir.'/'.$file)) {
                if (!unlink($dir.'/'.$file))
                    return false;
            }
            else if (is_dir($dir.'/'.$file)) {
                if (!self::deleteDir($dir.'/'.$file))
                    return false;
            }
        }
        closedir($dd);
        return true;
    }
}
