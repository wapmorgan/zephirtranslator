<?php
namespace wapmorgan\ZephirTranslator;

use PhpParser\Parser;
use PhpParser\Lexer;
use PhpParser\NodeTraverser;

use PhpParser\Node\Expr\BinaryOp\Concat;
use PhpParser\Node\Expr\FuncCall;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\StaticCall;
use PhpParser\Node\Stmt\Function_;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Interface_;

use \Exception;

function strintr($str1, $str2) {
    $length = min(strlen($str1), strlen($str2));
    $common = 0;
    for ($i = 0; $i < $length; $i++) {
        if ($str1[$i] == $str2[$i])
            $common++;
        else
            break;
    }
    return $common;
}

class Translator {
    protected $files;
    protected $usedNamespaces = array();
    protected $zephirProject;

    public function __construct($files) {
        $this->files = $files;
        $this->zephirProject = new ZephirProject;
    }

    public function parseFiles() {
        $parser = new Parser(new Lexer);

        foreach ($this->files as $file) {
            if ($_ENV['verbose'])
                echo '... parsing file '.$file.PHP_EOL;
            $content = file_get_contents($file);
            $stmts = $parser->parse($content);
            foreach ($stmts as $stmt) {
                if ($stmt->getType() == 'Stmt_Namespace') {
                    $this->usedNamespaces[] = (string)($stmt->name);
                }
            }
        }
        $this->zephirProject->namespace = ucfirst($this->resolveMostCommonPartOfNamespace());
        if (strpos($this->zephirProject->namespace, '\\') !== false)
            $this->zephirProject->namespace = strstr($this->zephirProject->namespace, '\\', true);
    }

    public function createConfiguration() {
        echo '[namespace] Project namespace: '.$this->zephirProject->namespace.', is it right? [Y/n]';
        $answer = fgets(STDIN);
        if (!in_array(trim(strtolower($answer)), array('', 'y', 'ye', 'yes'))) {
            echo '[namespace] Type project namespace: ';
            $namespace = trim(fgets(STDIN));
            $this->zephirProject->namespace = $namespace;
        }

        echo '[name] Type project name: ';
        $name = trim(fgets(STDIN));
        $this->zephirProject->name = $name;

        echo '[description] Describe project: ';
        $description = trim(fgets(STDIN));
        $this->zephirProject->description = $description;

        echo '[author] Type project author: ';
        $author = trim(fgets(STDIN));
        $this->zephirProject->author = $author;

        echo '[version] Type project version (a.b.c): ';
        $version = trim(fgets(STDIN));
        $this->zephirProject->version = $version;
    }

    public function translateFiles($outputDirectory, $fromDirectory) {
        $parser = new Parser(new Lexer);
        $traverser = new NodeTraverser();
        $traverser->addVisitor(new NamespaceConverter());

        foreach ($this->files as $file) {
            $target = $outputDirectory.DIRECTORY_SEPARATOR.substr($file, strlen($fromDirectory.DIRECTORY_SEPARATOR), strrpos($file, '.') - strlen($fromDirectory.DIRECTORY_SEPARATOR)).'.zep';
            if ($_ENV['verbose'])
                echo '... translating file '.$file.' into '.$target.PHP_EOL;
            $content = file_get_contents($file);
            $stmts = $parser->parse($content);
            $stmts = $traverser->traverse($stmts);
            try {
                $zephir = $this->translatePhpIntoZephir($stmts);
            } catch (Exception $e) {
                throw new Exception('Error while parsing: '.$e->getMessage().'; File: '.$file);
            }
            if (!is_dir(dirname($target)))
                mkdir(dirname($target), 0755, true);
            file_put_contents($target, $zephir);
        }
    }

    public function writeConfiguration($outputDirectory) {
        return file_put_contents($outputDirectory.'/config.json', json_encode((array)$this->zephirProject)) !== false;
    }

    protected function resolveMostCommonPartOfNamespace() {
        $common = null;
        foreach ($this->usedNamespaces as $i => $ns) {
            if ($i == 0)
                $common = $ns;
            else
                $common = substr($common, 0, strintr($common, $ns));
        }
        return $common;
    }

    protected function translatePhpIntoZephir(array $stmts) {
        $buffer = null;
        $buffer .= 'namespace '.(string)($stmts[0]->name).';'.PHP_EOL.PHP_EOL;
        $i = 0;
        while ($stmts[0]->stmts[$i]->getType() == 'Stmt_Use') {
            foreach ($stmts[0]->stmts[$i]->uses as $use) {
                $buffer .= 'use '.(string)($use->name).';'.PHP_EOL;
            }
            $i++;
        }
        $t = count($stmts[0]->stmts);
        for (; $i < $t; $i++) {
            switch ($stmts[0]->stmts[$i]->getType()) {
                case 'Stmt_Class':
                    $buffer .= $this->translateClass($stmts[0]->stmts[$i]);
                    break;
                case 'Stmt_Function':
                    $buffer .= $this->translateFunction($stmts[0]->stmts[$i]);
                    break;
                case 'Stmt_Interface':
                    $buffer .= $this->translateInterface($stmts[0]->stmts[$i]);
                    break;
                default:
                    echo 'Unknow root stmt'.PHP_EOL;
                    var_dump($stmts[0]->stmts[$i]);
                    break;
            }
        }
        // print_r($buffer);
        return $buffer;
    }

    protected function translateClass(Class_ $class) {
        $buffer = null;
        $buffer .= PHP_EOL.'class '.$class->name;
        if (isset($class->extends))
            $buffer .= ' extends '.(string)($class->extends);
        $buffer .= ' {'.PHP_EOL;
        $j = 0;
        while (isset($class->stmts[$j]) && $class->stmts[$j]->getType() == 'Stmt_Property') {
            $buffer .= '    ';
            switch ($class->stmts[$j]->type) {
                case Class_::MODIFIER_PUBLIC:
                    $buffer .= 'public';
                    break;
                case Class_::MODIFIER_PROTECTED;
                    $buffer .= 'protected';
                    break;
                case Class_::MODIFIER_PRIVATE;
                    $buffer .= 'private';
                    break;
            }
            $buffer .= ' '.$class->stmts[$j]->props[0]->name;
            if (!empty($class->stmts[$j]->props[0]->default)) {
                switch ($class->stmts[$j]->props[0]->default->getType()) {
                    case 'Expr_Array':
                        $buffer .= ' = [';
                        foreach ($class->stmts[$j]->props[0]->default->items as $_i => $item) {
                            if ($_i > 0) $buffer .= ', ';
                            switch ($item->value->getType()) {
                                case 'Scalar_String':
                                    $buffer .= '"'.str_replace('"', '\"', $item->value->value).'"';
                                    break;
                            }
                        }
                        $buffer .= ']';
                        break;
                }
            }
            $buffer .= ';'.PHP_EOL;
            $j++;
        }
        while (isset($class->stmts[$j]) && $class->stmts[$j]->getType() == 'Stmt_ClassMethod') {
            $buffer .= $this->translateClassMethod($class->stmts[$j]);
            $j++;
        }
        $buffer .= '}'.PHP_EOL;
        return $buffer;
    }

    protected function translateFunction(Function_ $function) {
        $buffer = null;
        $buffer .= PHP_EOL.'function '.$function->name.'(';
        foreach ($function->params as $_i => $param)  {
            if ($_i > 0) $buffer .= ', ';
            $buffer .= $param->name;
            if (!empty($param->default)) {
                switch ($param->default->getType()) {
                    case 'Expr_Array':
                        $buffer .= ' = [';
                        foreach ($param->default->items as $_i => $item) {
                            if ($_i > 0) $buffer .= ', ';
                            switch ($item->value->getType()) {
                                case 'Scalar_String':
                                    $buffer .= '"'.str_replace('"', '\"', $item->value->value).'"';
                                    break;
                            }
                        }
                        $buffer .= ']';
                        break;
                }
            }
        }
        $namesTable = array();
        $buffer .= ') {'.PHP_EOL;
        $buffer .= $this->translateCodeBlock($function->stmts, $namesTable, 1);
        $buffer .= '}'.PHP_EOL;
        return $buffer;
    }

    protected function translateClassMethod(ClassMethod $method) {
        $buffer = null;
        $buffer .= PHP_EOL.'    ';
        if ($method->type & Class_::MODIFIER_STATIC) {
            $buffer .= 'static ';
        }
        if ($method->type & Class_::MODIFIER_ABSTRACT) {
            $buffer .= 'abstract ';
        }
        if ($method->type & Class_::MODIFIER_PUBLIC) {
            $buffer .= 'public ';
        }
        if ($method->type & Class_::MODIFIER_PROTECTED) {
            $buffer .= 'protected ';
        }
        if ($method->type & Class_::MODIFIER_PRIVATE) {
            $buffer .= 'private ';
        }
        $buffer .= 'function '.$method->name.'(';
        foreach ($method->params as $_i => $param)  {
            if ($_i > 0) $buffer .= ', ';
            $buffer .= $param->name;
            if (!empty($param->default)) {
                switch ($param->default->getType()) {
                    case 'Expr_Array':
                        $buffer .= ' = [';
                        foreach ($param->default->items as $_i => $item) {
                            if ($_i > 0) $buffer .= ', ';
                            switch ($item->value->getType()) {
                                case 'Scalar_String':
                                    $buffer .= '"'.str_replace('"', '\"', $item->value->value).'"';
                                    break;
                            }
                        }
                        $buffer .= ']';
                        break;
                }
            }
        }
        if ($method->stmts === null)
            $buffer .= ');'.PHP_EOL;
        else {
            $buffer .= ') {'.PHP_EOL;
            $namesTable = array();
            $buffer .= $this->translateCodeBlock($method->stmts, $namesTable, 2);
            $buffer .= '    }'.PHP_EOL;
        }
        return $buffer;
    }

    protected function translateInterface(Interface_ $interface) {
        $buffer = null;
        $buffer .= PHP_EOL.'interface '.$interface->name;
        $buffer .= ' {'.PHP_EOL;
        var_dump($interface);
        $buffer .= '}'.PHP_EOL;
        return $buffer;
    }

    protected function translateCodeBlock(array $stmts, &$namesTable, $paddingLevel) {
        $buffer = null;
        foreach ($stmts as $stmt) {
            switch ($stmt->getType()) {
                case 'Stmt_If':
                    $buffer .= str_repeat('    ', $paddingLevel).'if ';
                    $buffer .= $this->translateCondition($stmt->cond);
                    $buffer .= ' {'.PHP_EOL;
                    $buffer .= $this->translateCodeBlock($stmt->stmts, $namesTable, $paddingLevel + 1);
                    $buffer .= str_repeat('    ', $paddingLevel).'}';
                    if (!empty($stmt->else)) {
                        $buffer .= ' else {'.PHP_EOL;
                        $buffer .= $this->translateCodeBlock($stmt->else->stmts, $namesTable, $paddingLevel + 1);
                        $buffer .= str_repeat('    ', $paddingLevel).'}';
                    }
                    $buffer .= PHP_EOL;
                    break;
                case 'Expr_PostInc':
                    $buffer .= str_repeat('    ', $paddingLevel).'let '.$stmt->var->name.'++;'.PHP_EOL;
                    break;
                case 'Stmt_Break':
                    $buffer .= str_repeat('    ', $paddingLevel).'break;'.PHP_EOL;
                    break;
                case 'Stmt_Return':
                    $buffer .= str_repeat('    ', $paddingLevel).'return ';
                    if (!empty($stmt->expr)) {
                        switch ($stmt->expr->getType()) {
                            case 'Expr_MethodCall':
                                $buffer .= $this->translateMethodCall($stmt->expr);
                                break;
                            case 'Expr_ConstFetch':
                                $buffer .= (string)($stmt->expr->name);
                                break;
                            case 'Expr_BinaryOp_Identical':
                                $buffer .= $this->translateCondition($stmt->expr);
                                break;
                            case 'Expr_Variable':
                                $buffer .= $stmt->expr->name;
                                break;
                            case 'Expr_PropertyFetch':
                                $buffer .= $this->buildVarName($stmt->expr);
                                break;
                            case 'Expr_BinaryOp_Concat':
                                $buffer .= $this->buildConcatenatedString($stmt->expr);
                                break;
                            case 'Expr_Cast_String':
                                $buffer .= '(string)'.$this->resolveExpression($stmt->expr);
                                break;
                            case 'Expr_BinaryOp_NotIdentical':
                                $buffer .= $this->translateCondition($stmt->expr);
                                break;
                            case 'Scalar_LNumber':
                                $buffer .= $this->resolveExpression($stmt->expr);
                                break;
                            case 'Expr_New':
                                $buffer .= 'new ';
                                if ($stmt->expr->class->getType() == 'Expr_Variable') {
                                    $buffer .= '{'.$this->buildVarName($stmt->expr->class).'}';
                                } else {
                                    $buffer .= (string)($stmt->expr->class);
                                }
                                $buffer .= '('.$this->buildArgsList($stmt->expr->args).')';
                                break;
                            case 'Expr_Cast_Bool':
                                $buffer .= '(bool)'.$this->resolveExpression($stmt->expr);
                                break;
                            case 'Expr_FuncCall':
                                $buffer .= $this->translateFunctionCall($stmt->expr);
                                break;
                            default:
                                echo 'Unknown return expression'.PHP_EOL;
                                var_dump($stmt->expr);
                                break;
                        }
                    }
                    $buffer .= ';'.PHP_EOL;
                    break;
                case 'Expr_Assign':
                        switch ($stmt->var->getType()) {
                            case 'Expr_PropertyFetch':
                                $buffer .= str_repeat('    ', $paddingLevel).'let '.$this->buildVarName($stmt->var).' = ';
                                break;
                            case 'Expr_Variable':
                                if (!isset($namesTable[$stmt->var->name])) {
                                    $namesTable[$stmt->var->name] = 1;
                                    $buffer .= str_repeat('    ', $paddingLevel).'var '.$stmt->var->name.';'.PHP_EOL;
                                }
                                $buffer .= str_repeat('    ', $paddingLevel).'let '.$stmt->var->name.' = ';
                                break;
                            case 'Expr_ArrayDimFetch':
                                $buffer .= str_repeat('    ', $paddingLevel).'let '.$this->buildVarName($stmt->var).' = ';
                                break;
                            default:
                                echo 'Unknown assign var'.PHP_EOL;
                                var_dump($stmt->var);
                                break;
                        }
                        switch ($stmt->expr->getType()) {
                            case 'Expr_FuncCall':
                                $buffer .= $this->translateFunctionCall($stmt->expr);
                                break;
                            case 'Scalar_LNumber':
                                $buffer .= $stmt->expr->value;
                                break;
                            case 'Expr_Variable':
                                $buffer .= $stmt->expr->name;
                                break;
                            case 'Expr_New':
                                $buffer .= 'new '.(string)($stmt->expr->class).'('.$this->buildArgsList($stmt->expr->args).')';
                                break;
                            case 'Expr_ConstFetch':
                                $buffer .= (string)($stmt->expr->name);
                                break;
                            case 'Expr_MethodCall':
                                $buffer .= $this->translateMethodCall($stmt->expr);
                                break;
                            case 'Expr_Cast_String':
                                $buffer .= '(string)'.$this->resolveExpression($stmt->expr);
                                break;
                            case 'Expr_BinaryOp_Concat':
                                $buffer .= $this->buildConcatenatedString($stmt->expr);
                                break;
                            case 'Expr_PropertyFetch':
                                $buffer .= $this->buildVarName($stmt->expr);
                                break;
                            case 'Expr_Array':
                                $buffer .= $this->buildArgsList($stmt->expr->items, true);
                                break;
                            case 'Expr_ArrayDimFetch':
                                $buffer .= $this->buildVarName($stmt->expr);
                                break;
                            case 'Expr_Ternary':
                                $buffer .= $this->translateCondition($stmt->expr->cond).' ? '.$this->resolveExpression($stmt->expr->if).' : '.$this->resolveExpression($stmt->expr->else);
                                break;
                            default:
                                echo 'Unknown assign expression.'.PHP_EOL;
                                var_dump($stmt->expr);
                                break;
                        }
                        $buffer .= ';'.PHP_EOL;
                    break;
                case 'Stmt_While':
                    // one optimization
                    if (isset($stmt->cond->left) && isset($stmt->cond->right) && $this->conditionHasAssignation($stmt->cond)) {
                        $assign = $this->extractAssignationFromCondition($stmt->cond);
                        $buffer .= $this->translateCodeBlock(array($assign), $namesTable, $paddingLevel);
                        $stmt->stmts[] = $assign;
                    }
                    $buffer .= str_repeat('    ', $paddingLevel).'while '.$this->translateCondition($stmt->cond).' {'.PHP_EOL;
                    $buffer .= $this->translateCodeBlock($stmt->stmts, $namesTable, $paddingLevel + 1);
                    $buffer .= str_repeat('    ', $paddingLevel).'}'.PHP_EOL;
                    break;
                case 'Stmt_For':
                    foreach ($stmt->init as $init) {
                        switch ($init->getType()) {
                            case 'Expr_Assign':
                                if (!isset($namesTable[$init->var->name])) {
                                    $namesTable[$init->var->name] = 1;
                                    $buffer .= str_repeat('    ', $paddingLevel).'var '.$init->var->name.' = ';
                                } else {
                                    $buffer .= str_repeat('    ', $paddingLevel).'let '.$init->var->name.' = ';
                                }
                                switch ($init->expr->getType()) {
                                    case 'Expr_FuncCall':
                                        $buffer .= $this->translateFunctionCall($init->expr);
                                        break;
                                    case 'Scalar_LNumber':
                                        $buffer .= $init->expr->value;
                                        break;
                                    case 'Expr_Variable':
                                        $buffer .= $init->name;
                                        break;
                                    default:
                                        echo 'Unknown for_assign expression.'.PHP_EOL;
                                        var_dump($init->expr);
                                        break;
                                }
                                $buffer .= ';'.PHP_EOL;
                                break;
                            default:
                                echo 'Unknow init'.PHP_EOL;
                                var_dump($init);
                                break;
                        }
                        $buffer .= str_repeat('    ', $paddingLevel).'while ';
                        if (count($stmt->cond) === 0) {
                            $buffer .= 'true';
                        } else {
                            foreach ($stmt->cond as $_i => $cond) {
                                if ($_i > 0) $buffer .= ' && ';
                                switch ($cond->getType()) {
                                    case 'Expr_BinaryOp_Smaller':
                                        switch ($cond->left->getType()) {
                                            case 'Expr_Variable':
                                                $buffer .= $cond->left->name;
                                                break;
                                        }
                                        $buffer .= ' > ';
                                        switch ($cond->right->getType()) {
                                            case 'Expr_Variable':
                                                $buffer .= $cond->right->name;
                                                break;
                                        }
                                        break;
                                }
                            }
                        }
                        $buffer .= ' {'.PHP_EOL;
                        $buffer .= $this->translateCodeBlock($stmt->stmts, $namesTable, $paddingLevel + 1);
                        $buffer .= $this->translateCodeBlock($stmt->loop, $namesTable, $paddingLevel + 1);
                        $buffer .= str_repeat('    ', $paddingLevel).'}'.PHP_EOL;
                    }
                    break;
                case 'Stmt_Continue':
                    $buffer .= str_repeat('    ', $paddingLevel).'continue;'.PHP_EOL;
                    break;
                case 'Expr_FuncCall':
                    $buffer .= str_repeat('    ', $paddingLevel).$this->translateFunctionCall($stmt).';'.PHP_EOL;
                    break;
                case 'Stmt_Switch':
                    $buffer .= str_repeat('    ', $paddingLevel).'switch ';
                    switch ($stmt->cond->getType()) {
                        case 'Expr_MethodCall':
                            $buffer .= $this->translateMethodCall($stmt->cond);
                            break;
                        case 'Expr_PropertyFetch':
                            $buffer .= $this->buildVarName($stmt->cond);
                            break;
                        case 'Expr_Variable':
                            $buffer .= $this->buildVarName($stmt->cond);
                            break;
                        default:
                            echo 'Unknown switch cond'.PHP_EOL;
                            var_dump($stmt->cond);
                            break;
                    }
                    $buffer .= ' {'.PHP_EOL;
                    foreach ($stmt->cases as $case) {
                        $buffer .= str_repeat('    ', $paddingLevel + 1).'case ';
                        if (!empty($case->cond)) {
                            switch ($case->cond->getType()) {
                                case 'Scalar_String':
                                    $buffer .= $this->resolveExpression($case->cond);
                                    break;
                                case 'Expr_ClassConstFetch':
                                    $buffer .= $this->resolveExpression($case->cond);
                                    break;
                                default:
                                    echo 'Unknown case cond'.PHP_EOL;
                                    var_dump($case->cond);
                                    break;
                            }
                            $buffer .= ':'.PHP_EOL;
                        } else {
                            $buffer .= 'default:'.PHP_EOL;
                        }
                        $buffer .= $this->translateCodeBlock($case->stmts, $namesTable, $paddingLevel + 2);
                    }
                    $buffer .= str_repeat('    ', $paddingLevel).'}'.PHP_EOL;
                    break;
                case 'Expr_AssignOp_Concat':
                    $buffer .= str_repeat('    ', $paddingLevel).'let ';
                    switch ($stmt->var->getType()) {
                        case 'Expr_Variable':
                            if (!isset($namesTable[$stmt->var->name])) {
                                $namesTable[$stmt->var->name] = 1;
                            }
                            $buffer .= $stmt->var->name;
                            break;
                        case 'Expr_ArrayDimFetch':
                            $buffer .= $this->buildVarName($stmt->var);
                            break;
                    }
                    $buffer .= ' .= '.$this->resolveExpression($stmt->expr).';'.PHP_EOL;
                    break;
                case 'Stmt_Foreach':
                    if (!empty($stmt->keyVar)) {
                        if (!isset($namesTable[$stmt->keyVar->name])) {
                            $namesTable[$stmt->keyVar->name] = 1;
                            $buffer .= str_repeat('    ', $paddingLevel).'var '.$stmt->keyVar->name.';'.PHP_EOL;
                        }
                    }
                    if (!empty($stmt->valueVar)) {
                        if (!isset($namesTable[$stmt->valueVar->name])) {
                            $namesTable[$stmt->valueVar->name] = 1;
                            $buffer .= str_repeat('    ', $paddingLevel).'var '.$stmt->valueVar->name.';'.PHP_EOL;
                        }
                    }
                    $buffer .= str_repeat('    ', $paddingLevel).'for '.(isset($stmt->keyVar) ? $stmt->keyVar->name : '_').', '.(isset($stmt->valueVar) ? $stmt->valueVar->name : '_').' in '.$this->resolveExpression($stmt->expr).' {'.PHP_EOL;
                    $buffer .= $this->translateCodeBlock($stmt->stmts, $namesTable, $paddingLevel + 1);
                    $buffer .= str_repeat('    ', $paddingLevel).'}'.PHP_EOL;
                    break;
                case 'Stmt_Echo':
                    $buffer .= str_repeat('    ', $paddingLevel).'echo ';
                    foreach ($stmt->exprs as $_i => $expr) {
                        if ($_i > 0) $buffer .= ',';
                        $buffer .= $this->resolveExpression($expr);
                    }
                    $buffer .= ';'.PHP_EOL;
                    break;
                case 'Stmt_TryCatch':
                    $buffer .= str_repeat('    ', $paddingLevel).'try {'.PHP_EOL;
                    $buffer .= $this->translateCodeBlock($stmt->stmts, $namesTable, $paddingLevel + 1);
                    foreach ($stmt->catches as $catch) {
                        $buffer .= str_repeat('    ', $paddingLevel).'} catch '.(string)($catch->type).', '.$catch->var.' {'.PHP_EOL;
                        $buffer .= $this->translateCodeBlock($catch->stmts, $namesTable, $paddingLevel + 1);
                    }
                    $buffer .= str_repeat('    ', $paddingLevel).'}'.PHP_EOL;

                    break;
                case 'Stmt_Throw':
                    $buffer .= str_repeat('    ', $paddingLevel).'throw '.$this->resolveExpression($stmt->expr).';'.PHP_EOL;
                    break;
                case 'Expr_MethodCall':
                    $buffer .= str_repeat('    ', $paddingLevel).$this->translateMethodCall($stmt).';'.PHP_EOL;
                    break;
                case 'Expr_StaticCall':
                    $buffer .= str_repeat('    ', $paddingLevel).$this->translateStaticCall($stmt).';'.PHP_EOL;
                    break;
                default:
                    echo 'Unknown stmt'.PHP_EOL;
                    var_dump($stmt);
                    break;
            }
        }
        return $buffer;
    }

    protected function translateMethodCall(MethodCall $call) {
        $buffer = null;
        // optimization for dynamic method call like this
        // $this->{'p'.$node->getType()}($node);
        if (is_object($call->name)) {
            $buffer .= 'call_user_func_array(['.$this->buildVarName($call->var).', '.$this->resolveExpression($call->name).'], ['.$this->buildArgsList($call->args).'])';
        } else {
            $buffer .= $this->buildVarName($call->var).'->'.$call->name.'('.$this->buildArgsList($call->args).')';
        }
        return $buffer;
    }

    protected function buildVarName($var) {
        $buffer = null;
        switch ($var->getType()) {
            case 'Expr_PropertyFetch':
                $buffer .= $this->buildVarName($var->var);
                $buffer .= '->';
                if (is_object($var->name))
                    $buffer .= '{'.$var->name->name.'}';
                else
                    $buffer .= $var->name;
                break;
            case 'Expr_Variable':
                $buffer .= $var->name;
                break;
            case 'Expr_ArrayDimFetch':
                $buffer .= $this->buildVarName($var->var).'['.(isset($var->dim) ? $this->resolveExpression($var->dim) : null).']';
                break;
            case 'Expr_StaticPropertyFetch':
                $buffer .= (string)($var->class).'::'.$var->name;
                break;
            default:
                echo 'Unknown var'.PHP_EOL;
                var_dump($var);
                break;
        }
        return $buffer;
    }

    protected function translateCondition($cond) {
        $buffer = null;
        switch ($cond->getType()) {
            case 'Expr_BinaryOp_Smaller':
                $buffer .= $this->resolveExpression($cond->left).' < '.$this->resolveExpression($cond->right);
                break;
            case 'Expr_BinaryOp_Equal':
                $buffer .= $this->resolveExpression($cond->left).' == '.$this->resolveExpression($cond->right);
                break;
            case 'Expr_BinaryOp_NotIdentical':
                $buffer .= $this->resolveExpression($cond->left).' !== '.$this->resolveExpression($cond->right);
                break;
            case 'Expr_BinaryOp_Identical':
                $buffer .= $this->resolveExpression($cond->left).' === '.$this->resolveExpression($cond->right);
                break;
            case 'Expr_BinaryOp_BooleanOr':
                $buffer .= $this->resolveExpression($cond->left).' || '.$this->resolveExpression($cond->right);
                break;
            case 'Expr_BooleanNot':
                $buffer .= '!'.$this->resolveExpression($cond->expr);
                break;
            case 'Expr_FuncCall':
                $buffer .= $this->translateFunctionCall($cond);
                break;
            case 'Expr_ArrayDimFetch':
                $buffer .= $this->resolveExpression($cond);
                break;
            case 'Expr_Variable':
                $buffer .= $cond->name;
                break;
            case 'Expr_BinaryOp_Greater':
                $buffer .= $this->resolveExpression($cond->left).' > '.$this->resolveExpression($cond->right);
                break;
            case 'Expr_MethodCall':
                $buffer .= $this->translateMethodCall($cond);
                break;
            case 'Expr_BinaryOp_BitwiseAnd':
                $buffer .= $this->resolveExpression($cond->left).' & '.$this->resolveExpression($cond->right);
                break;
            case 'Expr_BinaryOp_BooleanAnd':
                $buffer .= $this->resolveExpression($cond->left).' && '.$this->resolveExpression($cond->right);
                break;
            case 'Expr_Isset':
                $buffer .= $this->resolveExpression($cond);
                break;
            case 'Expr_Instanceof':
                $buffer .= 'is_a('.$this->resolveExpression($cond->expr).', "'.(string)($cond->class).'")';
                break;
            case 'Scalar_LNumber':
                $buffer .= $cond->value;
                break;
            case 'Expr_StaticPropertyFetch':
                $buffer .= (string)($cond->class).'::'.$cond->name;
                break;
            default:
                echo 'Unknown cond'.PHP_EOL;
                var_dump($cond);
                break;
        }
        return $buffer;
    }

    protected function resolveExpression($expr) {
        if (!is_object($expr))
            var_dump(debug_backtrace(0, 2), $expr);
        switch ($expr->getType()) {
            case 'Expr_Variable':
                return $expr->name;

            case 'Expr_ArrayDimFetch':
                return $this->buildVarName($expr->var).'['.$this->resolveExpression($expr->dim).']';

            case 'Expr_ConstFetch':
                return (string)($expr->name);

            case 'Expr_FuncCall':
                return $this->translateFunctionCall($expr);

            case 'Expr_MethodCall':
                return $this->translateMethodCall($expr);

            case 'Expr_StaticCall':
                return $this->translateStaticCall($expr);

            case 'Expr_BinaryOp_Plus':
                return $this->resolveExpression($expr->left).' + '.$this->resolveExpression($expr->right);

            case 'Scalar_LNumber':
                return $expr->value;

            case 'Expr_PropertyFetch':
                return $this->buildVarName($expr);

            case 'Expr_BinaryOp_Concat':
                return $this->buildConcatenatedString($expr);

            case 'Scalar_String':
                return '"'.str_replace('\\', '\\\\', $expr->value).'"';

            case 'Expr_Cast_String':
                return '(string)'.$this->resolveExpression($expr->expr);

            case 'Expr_Empty':
                return 'empty '.$this->buildVarName($expr->expr);

            case 'Expr_Isset':
                $buffer = null;
                foreach ($expr->vars as $_i => $var) {
                    if ($_i > 0) $buffer .= ' && ';
                    $buffer .= 'isset '.$this->buildVarName($var);
                }
                return $buffer;

            case 'Expr_ClassConstFetch':
                return (string)($expr->class).'::'.$expr->name;

            case 'Expr_BinaryOp_Equal':
                return $this->resolveExpression($expr->left).' == '.$this->resolveExpression($expr->right);

            case 'Expr_New':
                return 'new '.(string)($expr->class).'('.$this->buildArgsList($expr->args).')';

            case 'Expr_Ternary':
                return $this->translateCondition($expr->cond).' ? '.$this->resolveExpression($expr->if).' : '.$this->resolveExpression($expr->else);

            case 'Expr_StaticPropertyFetch':
                return (string)($expr->class).'::'.$expr->name;

            case 'Expr_BinaryOp_BooleanAnd':
                return $this->resolveExpression($expr->left).' && '.$this->resolveExpression($expr->right);

            case 'Expr_Instanceof':
                return 'is_a('.$this->resolveExpression($expr->expr).', "'.(string)($expr->class).'")';

            case 'Expr_BinaryOp_Identical':
                return $this->resolveExpression($expr->left).' === '.$this->resolveExpression($expr->right);

            default:
                echo 'Unknown resolve_expr'.PHP_EOL;
                var_dump($expr);
                break;
        }
    }

    protected function translateStaticCall(StaticCall $call) {
        $buffer = null;
        $buffer .= (string)($call->class).'::'.$call->name.'('.$this->buildArgsList($call->args).')';
        return $buffer;
    }

    protected function conditionHasAssignation($cond) {
        if (!is_object($cond->left))
            var_dump(debug_backtrace(0, 2), $cond);
        if ($cond->left->getType() == 'Expr_Assign')
            return true;
        else if ($cond->right->getType() == 'Expr_Assign')
            return true;
        else
            return false;
    }

    protected function extractAssignationFromCondition(&$cond) {
        if ($cond->left->getType() == 'Expr_Assign') {
            $assign = $cond->left;
            $cond->left = $assign->var;
        } else {
            $assign = $cond->right;
            $cond->right = $assign->var;
        }
        return $assign;
    }

    protected function translateFunctionCall(FuncCall $call) {
        $buffer = null;
        $buffer .= (string)($call->name).'('.$this->buildArgsList($call->args).')';
        return $buffer;
    }

    protected function buildArgsList(array $args, $withKeys = false) {
        $buffer = null;
        foreach ($args as $_i => $arg) {
            if ($_i > 0) $buffer .= ', ';
            if ($withKeys) {
                if (!empty($arg->key)) {
                    switch ($arg->key->getType()) {
                        case 'Scalar_String':
                            $buffer .= $this->resolveExpression($arg->key).':';
                            break;
                        default:
                            echo 'Unknown key'.PHP_EOL;
                            var_dump($arg->key);
                            break;
                    }
                }
            }
            switch ($arg->value->getType()) {
                case 'Expr_FuncCall':
                    $buffer .= $this->translateFunctionCall($arg->value);
                    break;
                case 'Expr_Variable':
                    $buffer .= $arg->value->name;
                    break;
                case 'Expr_PropertyFetch':
                    $buffer .= $this->buildVarName($arg->value);
                    break;
                case 'Expr_New':
                    $buffer .= 'new '.(string)($arg->value->class).'('.$this->buildArgsList($arg->value->args).')';
                    break;
                case 'Expr_ClassConstFetch':
                    $buffer .= (string)($arg->value->class).'::'.$arg->value->name;
                    break;
                case 'Expr_BinaryOp_Concat':
                    $buffer .= $this->buildConcatenatedString($arg->value);
                    break;
                case 'Scalar_String':
                    $buffer .= '"'.str_replace('\\', '\\\\', $arg->value->value).'"';
                    break;
                case 'Scalar_LNumber':
                    $buffer .= $arg->value->value;
                    break;
                case 'Expr_Array':
                    $buffer .= '['.$this->buildArgsList($arg->value->items, true).']';
                    break;
                case 'Expr_ConstFetch':
                    $buffer .= (string)($arg->value->name);
                    break;
                case 'Expr_BinaryOp_Minus':
                    $buffer .= $this->resolveExpression($arg->value->left).' - '.$this->resolveExpression($arg->value->right);
                    break;
                case 'Expr_BinaryOp_Plus':
                    $buffer .= $this->resolveExpression($arg->value->left).' + '.$this->resolveExpression($arg->value->right);
                    break;
                case 'Expr_ArrayDimFetch':
                    $buffer .= $this->buildVarName($arg->value);
                    break;
                case 'Expr_Cast_Array':
                    $buffer .= '(array)'.$this->resolveExpression($arg->value->expr);
                    break;
                case 'Expr_MethodCall':
                    $buffer .= $this->translateMethodCall($arg->value);
                    break;
                case 'Scalar_MagicConst_Dir':
                    $buffer .= '__DIR__';
                    break;
                case 'Expr_Cast_Int':
                    $buffer .= '(int)'.$this->resolveExpression($arg->value->expr);
                    break;
                case 'Scalar_MagicConst_Class':
                    $buffer .= '__CLASS__';
                    break;
                case 'Expr_Ternary':
                    $buffer .= $this->translateCondition($arg->value->cond).' ? '.$this->resolveExpression($arg->value->if).' : '.$this->resolveExpression($arg->value->else);
                    break;
                default:
                    echo 'Unknown argument.'.PHP_EOL;
                    var_dump($arg->value);
                    break;
            }
        }
        return $buffer;
    }

    protected function buildConcatenatedString(Concat $concat) {
        return $this->resolveExpression($concat->left).'.'.$this->resolveExpression($concat->right);
    }
}
